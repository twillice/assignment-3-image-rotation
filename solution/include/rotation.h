#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

enum rotation_status {
    ROTATION_OK,
    ROTATION_OUT_OF_MEMORY
};

enum rotation_status image_rotate(struct image *img, long angle);

#endif //IMAGE_TRANSFORMER_ROTATION_H
