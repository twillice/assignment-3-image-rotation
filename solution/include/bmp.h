#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status {
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_OPEN_ERROR,
    READ_OUT_OF_MEMORY
};

enum read_status from_bmp(FILE *fp, struct image *img);

enum write_status {
    WRITE_OK,
    WRITE_ERROR,
    WRITE_FILE_OPEN_ERROR,
    WRITE_OUT_OF_MEMORY
};

enum write_status to_bmp(FILE *fp, const struct image *img);

enum read_status read_bmp_from_file(const char *filename, struct image *img);

enum write_status write_bmp_to_file(const char *filename, const struct image *img);

#endif //IMAGE_TRANSFORMER_BMP_H
