#include "bmp.h"
#include "malloc.h"

static uint32_t _padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE *fp, struct image *img) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, fp) != 1)
        return READ_INVALID_HEADER;
    if (header.bfType != 0x4D42 || header.biBitCount != 24 || header.biCompression != 0)
        return READ_INVALID_SIGNATURE;
    fseek(fp, header.bOffBits, SEEK_SET);

    uint32_t width = header.biWidth, height = header.biHeight;
    uint32_t padding = _padding(width);
    *img = image_create(width, height);
    if (!img->data) return READ_OUT_OF_MEMORY;
    for (uint32_t row = 0; row < height; row++) {
        if (fread(&img->data[(height - row - 1) * width], sizeof(struct pixel), width, fp) != width ||
            fseek(fp, padding, SEEK_CUR)) {
            free(img->data);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *fp, const struct image *img) {
    uint32_t width = img->width, height = img->height;
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24
    };
    uint32_t padding = _padding(width);
    header.biSizeImage = (width * sizeof(struct pixel) + padding) * width;
    header.bfileSize = header.bOffBits + header.biSizeImage;

    if (fwrite(&header, sizeof(struct bmp_header), 1, fp) != 1)
        return WRITE_ERROR;
    for (uint32_t row = 0; row < height; row++) {
        if (fwrite(&img->data[(height - row - 1) * width], sizeof(struct pixel), width, fp) != width ||
            fwrite("\0\0\0", padding, 1, fp) != 1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum read_status read_bmp_from_file(const char *filename, struct image *img) {
    FILE *fp = fopen(filename, "rb");
    if (!fp) return READ_FILE_OPEN_ERROR;
    enum read_status status = from_bmp(fp, img);
    fclose(fp);
    return status;
}

enum write_status write_bmp_to_file(const char *filename, const struct image *img) {
    FILE *fp = fopen(filename, "wb");
    if (!fp) return WRITE_FILE_OPEN_ERROR;
    enum write_status status = to_bmp(fp, img);
    fflush(fp);
    return fclose(fp) ? WRITE_ERROR : status;
}
