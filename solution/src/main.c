#include "bmp.h"
#include "image.h"
#include "rotation.h"
#include <stdio.h>
#include <stdlib.h>

static int error(const char *message) {
    fprintf(stderr, "%s\n", message);
    return 1;
}

int main(int argc, char *argv[]) {
    if (argc != 4)
        return error("Invalid arguments. Usage: ./image-transformer <source-image> <transformed-image> <angle>.");

    char *invalid_angle;
    long angle = strtol(argv[3], &invalid_angle, 10);
    if (*invalid_angle || angle % 90 || labs(angle) > 270)
        return error("Incorrect angle. Possible values: 0, 90, -90, 180, -180, 270, -270.");

    struct image img;
    enum read_status rs = read_bmp_from_file(argv[1], &img);
    if (rs != READ_OK)
        return error("Error reading image from file.");

    if (image_rotate(&img, angle) != ROTATION_OK)
        return error("Error rotating image.");

    enum write_status ws = write_bmp_to_file(argv[2], &img);
    image_free(&img);
    if (ws != WRITE_OK)
        return error("Error writing image to file.");
    return 0;
}
