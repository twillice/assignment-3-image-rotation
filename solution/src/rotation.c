#include "image.h"
#include "rotation.h"

enum rotation_status image_rotate(struct image *img, long angle) {
    long rotations = ((angle + 360) % 360) / 90;
    if (!rotations) return ROTATION_OK;

    uint64_t width = img->width, height = img->height;
    struct image rotated;
    if (rotations == 2) {
        rotated = image_create(width, height);
        if (!rotated.data) return ROTATION_OUT_OF_MEMORY;
        for (uint32_t row = 0; row < height; row++)
            for (uint32_t col = 0; col < width; col++)
                rotated.data[row * width + width - col - 1] = img->data[(height - row - 1) * width + col];
    } else {
        rotated = image_create(height, width);
        if (!rotated.data) return ROTATION_OUT_OF_MEMORY;
        if (rotations == 1) {
            for (uint32_t row = 0; row < height; row++)
                for (uint32_t col = 0; col < width; col++)
                    rotated.data[height * col + height - row - 1] = img->data[width * row + col];
        } else {
            for (uint32_t row = 0; row < height; row++)
                for (uint32_t col = 0; col < width; col++)
                    rotated.data[height * (width - col - 1) + row] = img->data[width * row + col];
        }
    }

    image_free(img);
    *img = rotated;
    return ROTATION_OK;
}

//void image_rotate_right(struct image *img) {
//    uint64_t width = img->width, height = img->height;
//    struct image rotated = image_create(height, width);
//    for (uint32_t row = 0; row < height; row++)
//        for (uint32_t col = 0; col < width; col++)
//            rotated.data[height * col + height - row - 1] = img->data[width * row + col];
//    image_free(img);
//    *img = rotated;
//}

//void image_rotate_left(struct image *img) {
//    uint64_t width = img->width, height = img->height;
//    struct image rotated = image_create(height, width);
//    for (uint32_t row = 0; row < height; row++)
//        for (uint32_t col = 0; col < width; col++)
//            rotated.data[height * (width - col - 1) + row] = img->data[width * row + col];
//    image_free(img);
//    *img = rotated;
//}

//void image_rotate_180(struct image *img) {
//    uint64_t width = img->width, height = img->height;
//    struct image rotated = image_create(width, height);
//    for (uint32_t row = 0; row < height; row++)
//        for (uint32_t col = 0; col < width; col++)
//            rotated.data[row * width + width - col - 1] = img->data[(height - row - 1) * width + col];
//    image_free(img);
//    *img = rotated;
//}

//void image_rotate(struct image *img, long angle) {
//    long rotations = ((angle + 360) % 360) / 90;
//    if (rotations == 1) image_rotate_right(img);
//    else if (rotations == 2) image_rotate_180(img);
//    else if (rotations == 3) image_rotate_left(img);
//}

//void image_rotate(struct image *img, long angle) {
//    long rotations = ((angle + 360) % 360) / 90;
//    if (!rotations) return;
//
//    uint64_t width = img->width, height = img->height;
//    struct image rotated;
//    if (rotations == 2) rotated = image_create(width, height);
//    else rotated = image_create(height, width);
//
//    for (uint32_t row = 0; row < height; row++)
//        for (uint32_t col = 0; col < width; col++)
//            if (rotations == 1) rotated.data[height * col + height - row - 1] = img->data[width * row + col];
//            else if (rotations == 3) rotated.data[height * (width - col - 1) + row] = img->data[width * row + col];
//            else rotated.data[row * width + width - col - 1] = img->data[(height - row - 1) * width + col];
//
//    image_free(img);
//    *img = rotated;
//}
